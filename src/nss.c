/***
    This file is part of nss-hostslocal.
 
    nss-hostslocal is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation; either version 2 of the License,
    or (at your option) any later version.
 
    nss-hostslocal is distributed in the hope that it will be useful, but1
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.
 
    You should have received a copy of the GNU Lesser General Public License
    along with nss-hostslocal; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA.
***/

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <nss.h>
#include <stdio.h>
#include <stdlib.h>

// needed by files-hosts, normally included by nsswitch.h
#include <stdbool.h>

// include below wasn't easy, look at the README in /glibc/
// one of the following is needed by nsswitch.h inc. in files-hosts.
#define internal_function
//#define internal_function __attribute__ ((regparm (3), stdcall))
//#define internal_function __attribute ((regparm (3), stdcall))
#include "files-hosts.c"

/*
  The HOST_DB_LOOKUP macro in files-hosts.c l.302 defines the following prototype:
  * _nss_files_gethostbyname_r
  * _nss_files_gethostbyname2_r

  The DB_LOOKUP macro in files-XXX.c l.317 defines the following prototype:
  * _nss_files_get##name##_r
  It is called one by files-hosts.c l.312
  to generate _nss_files_gethostbyaddr_r

  * _nss_files_gethostbyname4_r is explicitely created l. 322 of files-hosts
*/

enum nss_status _nss_hostslocal_gethostbyname_r(
    const char *name,
    struct hostent *result,
    char *buffer,
    size_t buflen,
    int *errnop,
    int *h_errnop) {
  return _nss_files_gethostbyname_r ( name, result, buffer, buflen, errnop, h_errnop );
}

enum nss_status _nss_hostslocal_gethostbyaddr_r(
    const void *addr,
    int len,
    int af,
    struct hostent *result,
    char *buffer,
    size_t buflen,
    int *errnop,
    int *h_errnop) {
  return _nss_files_gethostbyaddr_r ( addr, len, af, result, buffer, buflen, errnop, h_errnop);
}

enum nss_status _nss_hostslocal_gethostbyname4_r (
						  const char *name,
						  struct gaih_addrtuple **pat,
						  char *buffer,
						  size_t buflen,
						  int *errnop,
						  int *herrnop,
						  int32_t *ttlp) {
  enum nss_status status = NSS_STATUS_UNAVAIL;
  return status;
  // unused
  // return _nss_files_gethostbyname4_r ( name, pat, buffer, buflen, errnop, herrnop, ttlp);
}

// this is the one which does HOST => IP resolution
enum nss_status _nss_hostslocal_gethostbyname2_r( const char *name,
						  int af,
						  struct hostent * result,
						  char *buffer,
						  size_t buflen,
						  int *errnop,
						  int *h_errnop) {
  return _nss_files_gethostbyname2_r ( name, af, result, buffer, buflen, errnop, h_errnop );
}
